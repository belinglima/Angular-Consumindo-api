import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./componentes/login/login.component";
import { CriarcondComponent } from './componentes/criarcond/criarcond.component';
import { ListarcondComponent } from './componentes/listarcond/listarcond.component';
import { EscrevermensagemComponent } from './componentes/escrevermensagem/escrevermensagem.component';
import { CaixaentradaComponent } from './componentes/caixaentrada/caixaentrada.component';
import { RelacaousuariosComponent } from './componentes/relacaousuarios/relacaousuarios.component';
import { UsuarioscondominioComponent } from './componentes/usuarioscondominio/usuarioscondominio.component';
import { PageNotFoundComponent } from './componentes/page-not-found/page-not-found.component';
import { HomeComponent } from './componentes/home/home.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { HeaderComponent } from './componentes/header/header.component';
import { RegistroComponent } from './componentes/registro/registro.component';

const routes: Routes = [
	{ path: 'login',      component: LoginComponent },
        { path: 'registro', component: RegistroComponent },
	{ path: 'criarcond',  component: CriarcondComponent },
	{ path: 'listarcond', component: ListarcondComponent },
	{ path: 'escrever',  component: EscrevermensagemComponent },
	{ path: 'caixaentrada', component: CaixaentradaComponent },
	{ path: 'relacaousuarios',  component: RelacaousuariosComponent },
	{ path: 'usuarioscondominio', component: UsuarioscondominioComponent },
	{ path: 'home', component: HomeComponent },	
	{ path: 'menu', component: MenuComponent },
	{ path: 'header', component: HeaderComponent },
	{ path: 'footer', component: FooterComponent },

        { path: '', redirectTo: '/login', pathMatch: 'full'},
        { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
