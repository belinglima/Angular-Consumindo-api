import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './componentes/login/login.component';
import { CriarcondComponent } from './componentes/criarcond/criarcond.component';
import { ListarcondComponent } from './componentes/listarcond/listarcond.component';
import { EscrevermensagemComponent } from './componentes/escrevermensagem/escrevermensagem.component';
import { CaixaentradaComponent } from './componentes/caixaentrada/caixaentrada.component';
import { RelacaousuariosComponent } from './componentes/relacaousuarios/relacaousuarios.component';
import { UsuarioscondominioComponent } from './componentes/usuarioscondominio/usuarioscondominio.component';
import { PageNotFoundComponent } from './componentes/page-not-found/page-not-found.component';
import { HomeComponent } from './componentes/home/home.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { HeaderComponent } from './componentes/header/header.component';
import { ConteudoComponent } from './componentes/conteudo/conteudo.component';
import { RegistroComponent } from './componentes/registro/registro.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CriarcondComponent,
    ListarcondComponent,
    EscrevermensagemComponent,
    CaixaentradaComponent,
    RelacaousuariosComponent,
    UsuarioscondominioComponent,
    PageNotFoundComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    HeaderComponent,
    ConteudoComponent,
    RegistroComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
