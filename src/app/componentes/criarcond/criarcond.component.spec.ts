import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarcondComponent } from './criarcond.component';

describe('CriarcondComponent', () => {
  let component: CriarcondComponent;
  let fixture: ComponentFixture<CriarcondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriarcondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarcondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
