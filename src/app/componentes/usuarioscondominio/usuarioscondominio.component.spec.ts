import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioscondominioComponent } from './usuarioscondominio.component';

describe('UsuarioscondominioComponent', () => {
  let component: UsuarioscondominioComponent;
  let fixture: ComponentFixture<UsuarioscondominioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioscondominioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioscondominioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
