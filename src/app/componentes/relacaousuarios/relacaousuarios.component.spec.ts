import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelacaousuariosComponent } from './relacaousuarios.component';

describe('RelacaousuariosComponent', () => {
  let component: RelacaousuariosComponent;
  let fixture: ComponentFixture<RelacaousuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelacaousuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelacaousuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
