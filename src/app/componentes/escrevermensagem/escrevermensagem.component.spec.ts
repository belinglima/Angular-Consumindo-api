import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscrevermensagemComponent } from './escrevermensagem.component';

describe('EscrevermensagemComponent', () => {
  let component: EscrevermensagemComponent;
  let fixture: ComponentFixture<EscrevermensagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscrevermensagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscrevermensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
