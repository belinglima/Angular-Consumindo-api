import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarcondComponent } from './listarcond.component';

describe('ListarcondComponent', () => {
  let component: ListarcondComponent;
  let fixture: ComponentFixture<ListarcondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarcondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarcondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
