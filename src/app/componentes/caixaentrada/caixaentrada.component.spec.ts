import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaixaentradaComponent } from './caixaentrada.component';

describe('CaixaentradaComponent', () => {
  let component: CaixaentradaComponent;
  let fixture: ComponentFixture<CaixaentradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaixaentradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaixaentradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
